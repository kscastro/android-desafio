package br.com.kaike.desafio.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import br.com.kaike.desafio.API.ApiGit;
import br.com.kaike.desafio.API.InterfaceApiGit;
import br.com.kaike.desafio.Adapter.PullRequestAdapter;
import br.com.kaike.desafio.Adapter.RecyclerViewEmptyAdapter;
import br.com.kaike.desafio.Model.Pull;
import br.com.kaike.desafio.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PullRequestActivity extends AppCompatActivity {

    private static final String TAG = "Test";
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private InterfaceApiGit interfaceApiGit;
    private PullRequestAdapter pullRequestAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull_request);

        recyclerView = findViewById(R.id.recycler_list_pullrequest);
        linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(new RecyclerViewEmptyAdapter());

        String user = getIntent().getStringExtra("user");
        String repository = getIntent().getStringExtra("repository");

        interfaceApiGit = ApiGit.getRetrofitApi().create(InterfaceApiGit.class);
        Call<List<Pull>> repos = interfaceApiGit.getPullRequest(user, repository);
        repos.enqueue(new Callback<List<Pull>>() {
            @Override
            public void onResponse(Call<List<Pull>> call, Response<List<Pull>> response) {
                if (response.isSuccessful()) {
                    List<Pull> pullArrayList = new ArrayList<>();

                    for (Pull c : response.body()) {

                        pullArrayList.add(c);
                    }

                    pullRequestAdapter = new PullRequestAdapter(pullArrayList, getApplicationContext());
                    recyclerView.setHasFixedSize(true);
                    recyclerView.setAdapter(pullRequestAdapter);
                    pullRequestAdapter.notifyDataSetChanged();

                }
            }

            @Override
            public void onFailure(Call<List<Pull>> call, Throwable t) {

            }
        });

    }
}
