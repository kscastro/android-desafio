package br.com.kaike.desafio.Adapter;

import android.content.Context;
import android.content.Intent;
import android.nfc.Tag;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.kaike.desafio.Activity.PullRequestActivity;
import br.com.kaike.desafio.Model.Repository;
import br.com.kaike.desafio.R;
import br.com.kaike.desafio.Model.OwnerRepository;


/**
 * Created by kaike on 11/04/2018.
 */

public class RepositoryAdapter extends RecyclerView.Adapter <RepositoryAdapter.MyViewHolder> {

    private List<Repository> repositories;
    private Context context;
    private static final String TAG = "Test";

    public RepositoryAdapter(List<Repository> repositories, Context context) {
        this.repositories = repositories;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_repository, parent,false);
        MyViewHolder myViewHolder = new MyViewHolder(view, context, repositories);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Repository repository = repositories.get(position);

        holder.txt_name.setText(repository.getUsername());
        holder.txt_forks.setText(repository.getForks());
        holder.txt_stars.setText(repository.getStars());
        holder.txt_description.setText(repository.getDescription());
        holder.txt_username.setText(repository.getOwnerRepository().getLogin());

        String image = repository.getOwnerRepository().getUrl();
        Picasso.with(this.context).load(image).into(holder.img_avatar);

    }

    @Override
    public int getItemCount() {
        return repositories.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView txt_name, txt_description, txt_username, txt_forks, txt_stars;
        ImageView img_avatar;

        List<Repository> repositories;
        Context context;

        public MyViewHolder(View itemView, Context context, List<Repository> repo) {
            super(itemView);

            this.repositories   = repo;
            this.context        = context;
            txt_name            = itemView.findViewById(R.id.tv_repo_name);
            txt_description     = itemView.findViewById(R.id.tv_repo_description);
            txt_username        = itemView.findViewById(R.id.tv_repo_username);
            txt_forks           = itemView.findViewById(R.id.tv_repo_forks);
            txt_stars           = itemView.findViewById(R.id.tv_repo_stars);
            img_avatar          = itemView.findViewById(R.id.img_repo_avatar);

            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {

            int position = getAdapterPosition();
            Repository repository = this.repositories.get(position);

            Intent intent = new Intent(context, PullRequestActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("user", repository.getUsername());
            intent.putExtra("repository", repository.getOwnerRepository().getLogin());

            Log.i(TAG, "user: " + repository.getOwnerRepository().getLogin());
            Log.i(TAG, "repository: " + repository.getUsername());
//pode falar
            this.context.startActivity(intent);

        }
    }
}
