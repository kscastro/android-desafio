package br.com.kaike.desafio.API;

import java.util.List;

import br.com.kaike.desafio.Model.GitHubCatalog;
import br.com.kaike.desafio.Model.Pull;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by kaike on 11/04/2018.
 */

public interface InterfaceApiGit {
    @GET("search/repositories?q=language:Java&sort=stars")
    Call<GitHubCatalog> getCatalog(@Query("page") int page);

    @GET("repos/{user}/{repository}/pulls")
    Call<List<Pull>> getPullRequest(@Path("repository") String repository, @Path("user") String user);

}
