package br.com.kaike.desafio.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by kaike on 11/04/2018.
 */

public class Repository {

    @SerializedName("owner")
    private OwnerRepository ownerRepository;

    @SerializedName("name")
    private String username;

    @SerializedName("description")
    private String description;

    @SerializedName("forks_count")
    private String forks;

    @SerializedName("stargazers_count")
    private String stars;

    public OwnerRepository getOwnerRepository() {
        return ownerRepository;
    }

    public void setOwnerRepository(OwnerRepository ownerRepository) {
        this.ownerRepository = ownerRepository;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getForks() {
        return forks;
    }

    public void setForks(String forks) {
        this.forks = forks;
    }

    public String getStars() {
        return stars;
    }

    public void setStars(String stars) {
        this.stars = stars;
    }

}
