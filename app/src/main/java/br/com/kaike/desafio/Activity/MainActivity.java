package br.com.kaike.desafio.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;

import br.com.kaike.desafio.API.ApiGit;
import br.com.kaike.desafio.API.InterfaceApiGit;
import br.com.kaike.desafio.Adapter.RecyclerViewEmptyAdapter;
import br.com.kaike.desafio.Adapter.RepositoryAdapter;
import br.com.kaike.desafio.Model.GitHubCatalog;
import br.com.kaike.desafio.Model.Repository;
import br.com.kaike.desafio.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ArrayList<Repository> repositories;
    private LinearLayoutManager linearLayoutManager;
    private RepositoryAdapter repositoryAdapter;

    private static final String TAG = "Test";

    private Call<GitHubCatalog> catalogCall;

    private InterfaceApiGit interfaceApiGit;

    private Boolean isScrooling = false;
    private int currentItens, totalItens, scrollOutItens;
    private int currentPage = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recycler_list);
        linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(new RecyclerViewEmptyAdapter());
        repositories = new ArrayList<>();

        loadMore();

      recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
           @Override
           public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                isScrooling = true;
            }

            @Override public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
               super.onScrolled(recyclerView, dx, dy);

                currentItens = linearLayoutManager.getChildCount();
               totalItens = linearLayoutManager.getItemCount();
                scrollOutItens = linearLayoutManager.findFirstVisibleItemPosition();


               if (isScrooling && (currentItens + scrollOutItens) >= totalItens) {
                   currentPage++;
                  isScrooling = false;
                    loadMore();            }
               if (dy < 0) {
                   currentPage--;
                    isScrooling = false;
                    loadMore();
                }
            }
        });
    } // end onCreate

    private void loadMore() {
        interfaceApiGit = ApiGit.getRetrofitApi().create(InterfaceApiGit.class);
        catalogCall = interfaceApiGit.getCatalog(currentPage);

        catalogCall.enqueue(new Callback<GitHubCatalog>() {
            @Override
            public void onResponse(Call<GitHubCatalog> call, Response<GitHubCatalog> response) {
                if (response.isSuccessful()) {

                    GitHubCatalog gitHubCatalog = response.body();

                    repositories = (ArrayList<Repository>) gitHubCatalog.getRepositories();

                    repositoryAdapter = new RepositoryAdapter(repositories, getApplicationContext());
                    recyclerView.setHasFixedSize(true);
                    recyclerView.setAdapter(repositoryAdapter);
                    repositoryAdapter.notifyDataSetChanged();
                    Log.i(TAG, "Incrementando..." + response.toString());
                    Log.i(TAG, "Carregando..." + response.message());
                } else {
                    Toast.makeText(MainActivity.this,
                            "Falha ao carregar lista de repositórios.",
                            Toast.LENGTH_SHORT)
                            .show();
                    Log.i(TAG , "retornou com erro" + response.toString());
                }
            }

            @Override
            public void onFailure(Call<GitHubCatalog> call, Throwable t) {
                Toast.makeText(MainActivity.this,
                        "Erro ao carregar lista de repositórios.",
                        Toast.LENGTH_SHORT)
                        .show();
                Log.i(TAG , "failure: " + t.getMessage());
            }
        });
    }
}
