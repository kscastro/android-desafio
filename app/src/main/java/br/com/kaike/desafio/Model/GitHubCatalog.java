package br.com.kaike.desafio.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by kaike on 11/04/2018.
 */

public class GitHubCatalog {
    @SerializedName("items")
    private List<Repository> repositories;

    private List<Pull> pullRequestList;

    public List<Repository> getRepositories() {
        return repositories;
    }

    public List<Pull> getPullRequestList() {
        return pullRequestList;
    }
}
