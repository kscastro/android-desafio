package br.com.kaike.desafio.Adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.kaike.desafio.Model.Pull;
import br.com.kaike.desafio.R;

/**
 * Created by kaike on 12/04/2018.
 */

public class PullRequestAdapter extends RecyclerView.Adapter <PullRequestAdapter.MyViewHolder> {

    private List<Pull> pullRequests;
    private Context context;

    public PullRequestAdapter(List<Pull> pullRequests, Context context) {
        this.pullRequests = pullRequests;
        this.context = context;
    }

    @Override
    public PullRequestAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_pullrequest, parent,false);
        PullRequestAdapter.MyViewHolder myViewHolder = new PullRequestAdapter.MyViewHolder(view, context, pullRequests);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Pull pull = pullRequests.get(position);

        holder.txt_description.setText(pull.getBody());
        holder.txt_pull_request_name.setText(pull.getTitle());
        holder.txt_username.setText(pull.getUser().getLogin());
        holder.txt_pull_date.setText(pull.getCreatedAt());

        String image = pull.getUser().getAvatarUrl();
        Picasso.with(this.context).load(image).into(holder.img_avatar);

        String url = pull.getHtmlUrl();

    }

    @Override
    public int getItemCount() {
        return pullRequests.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView txt_pull_request_name, txt_description, txt_username, txt_pull_date;
        ImageView img_avatar;

        List<Pull> pullRequests;
        Context context;

        public MyViewHolder(View itemView, Context context, List<Pull> pullRequests) {
            super(itemView);

            this.pullRequests        = pullRequests;
            this.context             = context;
            txt_pull_request_name    = itemView.findViewById(R.id.tv_pull_request_name);
            txt_description          = itemView.findViewById(R.id.tv_pull_description);
            txt_username             = itemView.findViewById(R.id.tv_pull_username);
            txt_pull_date            = itemView.findViewById(R.id.tv_pull_date);
            img_avatar               = itemView.findViewById(R.id.img_pull_avatar);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

            // Setting link when pressed pull request it will open browser on official GitHub Page

            int position = getAdapterPosition();
            Pull pull = this.pullRequests.get(position);
            String url = pull.getHtmlUrl();

            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(url));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);

        }


    }
}