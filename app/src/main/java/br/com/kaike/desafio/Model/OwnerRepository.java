package br.com.kaike.desafio.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by kaike on 11/04/2018.
 */

public class OwnerRepository {

    @SerializedName("login")
    private String login;

    @SerializedName("avatar_url")
    private String url;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


}
